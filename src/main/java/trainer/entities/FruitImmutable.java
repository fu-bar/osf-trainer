package trainer.entities;

import osf.modules.elm.OSFImmutableEntity;
import osf.objects.annotations.ClassMetadata;

import java.util.UUID;

@ClassMetadata(owner = "trainer", twinEntityType = FruitWritable.class)
public class FruitImmutable extends OSFImmutableEntity {
    private String name;
    private int price;

    /**
     * Instantiates a new Entity base.
     * Construction must be initiated exclusively by the ELM
     *
     * @param creationToken Part of internal verification, the is compared with an ELM value (on no-match an exception is thrown).
     */
    public FruitImmutable(UUID creationToken) {
        super(creationToken);
    }

    public FruitImmutable(UUID creationToken, FruitWritable source) {
        super(creationToken);
        this.name = source.getName();
        this.price = source.getPrice();
    }

    public FruitImmutable(UUID creationToken, FruitImmutable source) {
        super(creationToken);
        this.name = source.getName();
        this.price = source.getPrice();
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }
}
