package trainer.entities;

import osf.modules.elm.OSFWritableEntity;
import osf.objects.annotations.ClassMetadata;

import java.util.UUID;

@ClassMetadata(owner = "trainer", twinEntityType = FruitImmutable.class)
public class FruitWritable extends OSFWritableEntity {
    private String name;
    private int price;

    /**
     * Instantiates a new Entity base.
     * Construction must be initiated exclusively by the ELM
     *
     * @param creationToken Part of internal verification, the is compared with an ELM value (on no-match an exception is thrown).
     */
    public FruitWritable(UUID creationToken) {
        super(creationToken);
    }

    public FruitWritable(UUID creationToken, FruitImmutable source) {
        super(creationToken);
        this.name = source.getName();
        this.price = source.getPrice();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
