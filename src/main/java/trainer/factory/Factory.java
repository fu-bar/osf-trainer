package trainer.factory;

import osf.modules.bus.api.shared.SystemBusInterface;
import osf.modules.configuration.ConfProvider;
import osf.modules.configuration.ConfigManager;
import osf.modules.configuration.interfaces.ConfProviderInterface;
import osf.modules.configuration.interfaces.ConfigManagerInterface;
import osf.modules.configuration.readers.json.JSONConfReader;
import osf.modules.configuration.readers.json.JSONReaderSettings;
import osf.modules.contextprovider.ContextProvider;
import osf.modules.elm.ELM;
import osf.modules.elm.cache.MetricsReporterInterface;
import osf.modules.elm.utilities.BlackListingHelper;
import osf.modules.notifications.NotificationManager;
import osf.modules.synchronization.base.datamanager.factory.DataManagerFactory;
import osf.modules.synchronization.interfaces.OperationManager;
import osf.modules.synchronization.interfaces.TimeProviderBase;
import osf.shared.interfaces.LogProviderInterface;
import ota.communication.shared.interfaces.CommunicationModuleInterface;
import trainer.classes.*;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static trainer.structures.Constants.BUS_UDP_TOPIC;

public class Factory {
    private static final NotificationManager NOTIFICATION_MANAGER = new NotificationManager(new LogProvider());
    private static final MetricsReporterInterface METRICS_REPORTER = new MetricsReporter();
    private static final ContextProvider CONTEXT_PROVIDER = new ContextProviderTrainer();
    private static final TimeProviderBase TIME_PROVIDER = new TimeProvider();
    private static final OperationManager OPERATION_MANAGER = new OperationManagerTrainer();
    private static final LogProviderInterface LOG_PROVIDER = new LogProvider();
    private static final BlackListingHelper BLACK_LISTING_HELPER = new BlackListingHelper();
    private static final SystemBusInterface SYSTEM_BUS = new Bus(NOTIFICATION_MANAGER, BUS_UDP_TOPIC);
    private static final Map<String, ELM> serviceIdToELM = new HashMap<>();
    private static final SystemInfo systemInfo = new SystemInfo();
    private static ConfigManagerInterface configManager;

    private Factory() {
        // Utility Class
    }

    public static MetricsReporterInterface getMetricsReporter() {
        return METRICS_REPORTER;
    }

    public static NotificationManager getNotificationManager() {
        return NOTIFICATION_MANAGER;
    }

    public static ContextProvider getContextProvider() {
        return CONTEXT_PROVIDER;
    }

    public static TimeProviderBase getTimeProvider() {
        return TIME_PROVIDER;
    }

    public static OperationManager getOperationManager() {
        return OPERATION_MANAGER;
    }

    public static ConfProviderInterface getConfigProvider() {
        if (configManager == null) {
            JSONReaderSettings jsonReaderSettings = new JSONReaderSettings();
            jsonReaderSettings.setJsonFilesToRead(List.of(Path.of("config", "json", "config.json")));
            JSONConfReader confReader = new JSONConfReader(jsonReaderSettings);
            configManager = new ConfigManager(List.of(confReader), systemInfo);
        }

        return new ConfProvider(systemInfo, configManager);

    }

    public static LogProviderInterface getLogProvider() {
        return LOG_PROVIDER;
    }

    public static SystemBusInterface getBUS() {
        return SYSTEM_BUS;
    }

    public static BlackListingHelper getBlackListingHelper() {
        return BLACK_LISTING_HELPER;
    }

    public static ELM getELM(String serviceId) {
        serviceIdToELM.putIfAbsent(serviceId, new ELM(serviceId, Factory.getMetricsReporter(), Factory.getNotificationManager(), Factory.getContextProvider(), Factory.getTimeProvider(), Factory.getOperationManager(), Factory.getDataManagerFactory(), Factory.getConfigProvider(), Factory.getBlackListingHelper()));
        return serviceIdToELM.get(serviceId);
    }

    public static DataManagerFactory getDataManagerFactory() {
        return new DataManagerFactory(getNotificationManager(), getConfigProvider(), getLogProvider());
    }

    public static CommunicationModuleInterface getCommunicationModule() {
        return new CommunicationModule();
    }


}
