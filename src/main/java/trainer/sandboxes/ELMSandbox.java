package trainer.sandboxes;


import org.apache.logging.log4j.Logger;
import osf.modules.elm.ELM;
import osf.objects.structures.CreationContext;
import trainer.entities.FruitImmutable;
import trainer.entities.FruitWritable;
import trainer.factory.Factory;

public class ELMSandbox {
    public static void main(String[] args) throws InterruptedException {
        Logger log = Factory.getLogProvider().getLogger(ELMSandbox.class);
        ELM elm = Factory.getELM("trainer");
        FruitWritable fruitWritable = elm.getWritableFor(FruitImmutable.class);
        FruitImmutable fruitImmutable = (FruitImmutable) elm.submit(
                fruitWritable,
                CreationContext.DEVELOPER
        );
        for (int iteration = 0; iteration < 100; iteration++) {
            log.debug("Iteration {}", iteration + 1);
            fruitWritable = elm.getWritableFor(fruitImmutable);
            elm.submit(fruitWritable, CreationContext.DEVELOPER);
            log.debug("Total items in elm: {}", elm.getAll().size());
            Thread.sleep(100);
        }
        System.exit(0);
    }
}
