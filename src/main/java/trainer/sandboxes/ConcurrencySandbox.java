package trainer.sandboxes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.modules.concurrency.OSFConcurrency;
import trainer.factory.Factory;
import trainer.sandboxes.samples.tasks.FruitTask;
import trainer.sandboxes.samples.tasks.MarioTask;

public class ConcurrencySandbox {
    private static final Logger log = LogManager.getLogger(ConcurrencySandbox.class);

    public static void main(String[] args) throws InterruptedException {
        FruitTask fruitTask = new FruitTask("Fruit-Task");
        MarioTask marioTask = new MarioTask("Mario-Task");
        OSFConcurrency osfConcurrency = new OSFConcurrency(Factory.getConfigProvider());
        osfConcurrency.runConcurrently(fruitTask);
        osfConcurrency.runConcurrently(marioTask);
        Thread.sleep(5000);
        log.info("Stopping OSFConcurrency");
        osfConcurrency.stop();
    }
}
