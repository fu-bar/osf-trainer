package trainer.sandboxes.samples.tasks;

import osf.modules.concurrency.structures.OSFTask;

public class MarioTask extends OSFTask {
    public MarioTask(String name) {
        super(name);
    }

    @Override
    public void onSuccess() {
        log.info("[SUCCESS] " + this.getClass().getSimpleName());
    }

    @Override
    public void onFailure(Throwable cause) {
        log.error("[FAILURE] " + this.getClass().getSimpleName(), cause);
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            log.debug("Mario: " + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                log.warn("Interrupted on iteration {} out of 100", i);
                return;
            }
        }
    }
}
