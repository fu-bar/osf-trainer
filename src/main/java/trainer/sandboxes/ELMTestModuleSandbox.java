package trainer.sandboxes;

import com.fasterxml.jackson.core.JsonProcessingException;
import ota.test.elm.ELMTestModule;
import trainer.factory.Factory;

public class ELMTestModuleSandbox {
    public static void main(String[] args) throws JsonProcessingException {
        ELMTestModule elmTestModule = new ELMTestModule(
                Factory.getELM("trainer"),
                Factory.getCommunicationModule(),
                Factory.getLogProvider()
        );

        elmTestModule.getLast(
                "{\"id\": \"00000000-0000-0000-0000-000000000001\"}",
                10,
                "primary");

        elmTestModule.countSystemIds("primary");

        System.exit(0);
    }
}
