package trainer.sandboxes;

import osf.modules.elm.ELM;
import osf.modules.elm.OSFImmutableEntity;
import osf.objects.structures.CreationContext;
import trainer.classes.TrainerClass;
import trainer.entities.FruitImmutable;
import trainer.entities.FruitWritable;
import trainer.factory.Factory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GCSandbox extends TrainerClass {

    private final ELM elm;
    private static final int POPULATION_INTERVAL_MILLISECONDS = 1000;
    private static final int NUM_OF_SYSTEM_IDS = 2;
    private static final List<OSFImmutableEntity> entities = new ArrayList<>();
    private final ScheduledExecutorService populationExecutor;

    public GCSandbox(ELM elm) {
        log.info("Starting ELM population");
        this.elm = elm;
        for (int i = 0; i < NUM_OF_SYSTEM_IDS; i++) {
            entities.add(newFruit(null));
        }
        this.populationExecutor = Executors.newSingleThreadScheduledExecutor();
        this.populationExecutor.scheduleAtFixedRate(
                this::populate,
                0,
                POPULATION_INTERVAL_MILLISECONDS,
                TimeUnit.MILLISECONDS
        );
    }

    public static void main(String[] args) throws InterruptedException {
        int secondsToRun = 60;
        GCSandbox gcSandbox = new GCSandbox(Factory.getELM("GCSandbox"));
        Thread.sleep(secondsToRun * 1000L);
        gcSandbox.stop();
    }

    private void populate() {
        var entitiesCopy = List.copyOf(entities);
        entitiesCopy.forEach(entity -> entities.add(newFruit(entity)));
        entities.removeAll(entitiesCopy);
        logInfo();
    }

    private void logInfo() {
        log.info("Size: {}, SystemIds: {}", elm.size(), elm.countSystemIds());
    }

    private OSFImmutableEntity newFruit(OSFImmutableEntity base) {
        FruitWritable fruitWritable;
        fruitWritable = base == null ? elm.getWritableFor(FruitImmutable.class) : elm.getWritableFor(base);
        return elm.submit(
                fruitWritable,
                CreationContext.DEVELOPER
        );
    }

    public void stop() {
        this.populationExecutor.shutdownNow();
        log.info("Goodbye");
    }
}
