package trainer.sandboxes;

import org.apache.logging.log4j.Logger;
import osf.modules.configuration.interfaces.ConfProviderInterface;
import trainer.factory.Factory;

public class ConfigSandbox {
    public static void main(String[] args) {
        Logger log = Factory.getLogProvider().getLogger(ConfigSandbox.class);
        ConfProviderInterface configProvider = Factory.getConfigProvider();
        String keyToGet = "someKey";
        int myValue = configProvider.get(keyToGet).asInt();
        log.info("The value of {} is {}", keyToGet, myValue);
        System.exit(0);
    }
}
