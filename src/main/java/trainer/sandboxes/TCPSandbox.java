package trainer.sandboxes;

import trainer.classes.TCPCommunicator;
import trainer.structures.Fruit;
import trainer.structures.NetworkTarget;
import trainer.structures.TCPMessageConsumer;

public class TCPSandbox {
    private static void transmit(TCPCommunicator tcpCommunicator) {
        tcpCommunicator.transmit(new Fruit("Apple", 7));
        tcpCommunicator.transmit(new Fruit("Orange", 8));
        tcpCommunicator.transmit(new Fruit("Grape", 9));
    }

    public static void main(String[] args) throws InterruptedException {
        int objectsServerPort = 8080;
        int bytesServerPort = 9090;
        String serverHost = "127.0.0.1";
        TCPMessageConsumer consumer = new TCPMessageConsumer();

        TCPCommunicator tcpCommunicator = new TCPCommunicator();
        tcpCommunicator.addMessageConsumer(consumer);
        tcpCommunicator.startServer(objectsServerPort);
        tcpCommunicator.connect(new NetworkTarget(serverHost, objectsServerPort));

        TCPCommunicator bytesTCPCommunicator = new TCPCommunicator(true);
        bytesTCPCommunicator.startServer(bytesServerPort);
        bytesTCPCommunicator.addMessageConsumer(consumer);
        bytesTCPCommunicator.connect(new NetworkTarget(serverHost, bytesServerPort));

        transmit(tcpCommunicator);
        transmit(bytesTCPCommunicator);

        Thread.sleep(1000);
        System.exit(0);

    }
}
