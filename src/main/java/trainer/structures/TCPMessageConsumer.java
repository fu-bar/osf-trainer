package trainer.structures;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.Logger;
import trainer.classes.TrainerClass;
import trainer.factory.Factory;

import java.util.function.Consumer;

public class TCPMessageConsumer implements Consumer<TCPMessage> {
    private final Logger log;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public TCPMessageConsumer() {
        this.log = Factory.getLogProvider().getLogger(TCPMessageConsumer.class);
    }

    @Override
    public void accept(TCPMessage tcpMessage) {
        try {
            log.info("[Nice] Got a TCP message from {}\n{}", tcpMessage.getClientId(), objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tcpMessage));
        } catch (JsonProcessingException e) {
            TrainerClass.handleStaticError(e);
        }
    }
}
