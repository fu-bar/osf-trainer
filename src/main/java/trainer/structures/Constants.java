package trainer.structures;

public class Constants {
    public final static String BUS_UDP_TOPIC = "BUS_UDP_TOPIC";
    public final static int COMMUNICATOR_BUFFER_SIZE = 65536;
}
