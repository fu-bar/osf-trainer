package trainer.structures;

import java.io.Serializable;

public class TCPMessage implements Serializable {
    private final String clientId;
    private final Object object;

    public TCPMessage(String clientId, Serializable object) {
        this.clientId = clientId;
        this.object = object;
    }

    public String getClientId() {
        return clientId;
    }

    public Serializable getObject() {
        return (Serializable) object;
    }
}
