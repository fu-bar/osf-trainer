package trainer.utilities;

import com.google.common.collect.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import trainer.testing.structures.Recorder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class Excel {
    private static final int HEADER_ROW_INDEX = 0;
    private static final int TIMESTAMP_COLUMN_INDEX = 0;
    private static final int TIME_DELTA_COLUMN_INDEX = 1;
    private static final Logger log = LogManager.getLogger(Excel.class);
    private static final String DATE_FORMAT = "d/m/yyyy h:mm:ss";

    private Excel() {
        // Static class
    }

    public static void saveExcel(Collection<Recorder> recorders, File targetFile) {
        Map<String, Integer> columnNameToIndex = new HashMap<>();
        columnNameToIndex.put("Timestamp", TIMESTAMP_COLUMN_INDEX);
        columnNameToIndex.put("TimeDelta(Ns)", TIME_DELTA_COLUMN_INDEX);

        Workbook workbook = new XSSFWorkbook();

        CreationHelper createHelper = workbook.getCreationHelper();

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

        CellStyle dataCellStyle = workbook.createCellStyle();
        dataCellStyle.setAlignment(HorizontalAlignment.CENTER);

        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setAlignment(HorizontalAlignment.CENTER);
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat(DATE_FORMAT));

        for (Recorder recorder : recorders) {

            columnNameToIndex.clear();
            List<Object> values = Arrays.asList(recorder.getRecords().values().toArray());
            HashSet<String> mapKeys = new HashSet<>();
            for (int i = 1; i <= values.size(); i++) {
                var value = values.get(i - 1);
                if (value instanceof Map) {
                    mapKeys.addAll(((Map<String, Object>) value).keySet());
                }
            }
            recorder.getRecords().columnKeySet().stream().sorted().forEach(key -> columnNameToIndex.put(key, columnNameToIndex.keySet().size() + 2));
            List<String> sortedCustomKeys = mapKeys.stream().sorted(String::compareToIgnoreCase).collect(Collectors.toList());
            sortedCustomKeys.forEach(key -> {
                if (!columnNameToIndex.containsKey(key) && !key.equals("")) {
                    columnNameToIndex.put(key, TIME_DELTA_COLUMN_INDEX + columnNameToIndex.keySet().size() + 1);
                }
            });

            String sheetName = recorder.getId();  // Sheet per recorder
            Sheet sheet = workbook.createSheet(sheetName);


            Row headerRow = sheet.createRow(Excel.HEADER_ROW_INDEX);
            Cell headerCell = headerRow.createCell(TIMESTAMP_COLUMN_INDEX);
            headerCell.setCellValue("Timestamp");
            headerCell.setCellStyle(headerCellStyle);
            headerCell = headerRow.createCell(TIME_DELTA_COLUMN_INDEX);
            headerCell.setCellValue("TimeDelta(Ns)");
            headerCell.setCellStyle(headerCellStyle);

            Table<Instant, String, Object> recordEntries = recorder.getRecords();

            List<String> columnNames = columnNameToIndex.keySet().stream().sorted().filter(s -> !s.equals("")).collect(Collectors.toList());
            // Prepare column headers
            for (int i = 1; i <= columnNames.size(); i++) {
                String columnName = columnNames.get(i - 1);
                int columnIndex = TIME_DELTA_COLUMN_INDEX + i;
                columnNameToIndex.put(columnName, columnIndex);
                headerCell = headerRow.createCell(columnIndex);
                headerCell.setCellStyle(headerCellStyle);
                headerCell.setCellValue(columnName);
            }

            var recordsChronologically = recordEntries.cellSet()
                    .stream()
                    .sorted(Comparator.comparing(Table.Cell::getRowKey))
                    .collect(Collectors.toList());

            for (int rowIndex = 1; rowIndex <= recordsChronologically.size(); rowIndex++) {
                int recordIndex = rowIndex - 1;
                var row = recordsChronologically.get(recordIndex);
                Row entry = sheet.createRow(rowIndex);
                Cell cell = entry.createCell(TIMESTAMP_COLUMN_INDEX);
                cell.setCellValue(Date.from(row.getRowKey()));
                cell.setCellStyle(dateCellStyle);
                cell = entry.createCell(TIME_DELTA_COLUMN_INDEX);
                cell.setCellStyle(dataCellStyle);
                if (recordIndex != 0) {
                    long timeDeltaNano = Duration.between(recordsChronologically.get(recordIndex - 1).getRowKey(), row.getRowKey()).toNanos();
                    cell.setCellValue(timeDeltaNano);
                    cell.setCellStyle(dataCellStyle);
                } else {
                    cell.setCellValue(0);
                    cell.setCellStyle(dataCellStyle);
                }
                Object value = row.getValue();
                if (row.getColumnKey().equals("")) {
                    Map<String, Object> rowMap = (Map<String, Object>) row.getValue();
                    (rowMap).keySet().forEach(key -> {
                        Cell mapCell = entry.createCell(columnNameToIndex.get(key));
                        mapCell.setCellValue(JsonSerialization.toJson(rowMap.get(key)));
                        mapCell.setCellStyle(dataCellStyle);
                    });
                } else {
                    cell = entry.createCell(columnNameToIndex.get(row.getColumnKey()));
                    cell.setCellValue(JsonSerialization.toJson(value));
                    cell.setCellStyle(dataCellStyle);
                }
            }

            for (int i = 0; i < columnNames.size() + 2; i++) {
                sheet.autoSizeColumn(i);
            }

            sheet.createFreezePane(0, 1);
        }
        String fileLocation = targetFile.getAbsolutePath();

        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(fileLocation);
            workbook.write(outputStream);
            workbook.close();
            log.debug("Finished writing to {}", fileLocation);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
