package trainer.utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;

public class Reflection {
    private final static Logger log = LogManager.getLogger(Reflection.class);

    public static <T> T getPrivateField(Object object, String fieldName) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            Object value = field.get(null);
            return (T) value;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException("Failed accessing: " + fieldName, e);
        }
    }

    private Reflection() {
        // static
    }
}
