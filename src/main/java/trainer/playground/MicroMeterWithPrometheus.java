package trainer.playground;

import com.sun.net.httpserver.HttpServer;
import io.micrometer.core.instrument.Counter;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import io.prometheus.client.exporter.common.TextFormat;
import org.apache.logging.log4j.Logger;
import trainer.factory.Factory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class MicroMeterWithPrometheus {
    private static final String COUNTER_NAME = "myCounter";
    private static final int PROMETHEUS_PORT = 8080;
    private static final Logger log = Factory.getLogProvider().getLogger(MicroMeterWithPrometheus.class);
    private static HttpServer server;

    public static void main(String[] args) throws InterruptedException {
        PrometheusMeterRegistry prometheusRegistry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
        createPrometheusEndpoint(prometheusRegistry);
        Counter.builder(COUNTER_NAME).baseUnit("My unit").description("My Description").tag("MyKey", "MyValue").register(prometheusRegistry);
        Scanner keyboard = new Scanner(System.in);
        log.debug("Type something and press enter to exit.");
        for (int i = 0; i < 360; i++) {
            prometheusRegistry.get(COUNTER_NAME).counter().increment();
            TimeUnit.SECONDS.sleep(1);
            log.debug("Counter = {}", i);
        }
        keyboard.next();
        System.exit(0);
    }

    private static void createPrometheusEndpoint(PrometheusMeterRegistry prometheusRegistry) {
        try {
            server = HttpServer.create(new InetSocketAddress(PROMETHEUS_PORT), 0);
            log.info("Serving metrics at http://127.0.0.1:{}/metrics", PROMETHEUS_PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.createContext("/metrics", httpExchange -> {
            try (OutputStream os = httpExchange.getResponseBody()) {
                String response = prometheusRegistry.scrape(TextFormat.CONTENT_TYPE_OPENMETRICS_100);
                httpExchange.sendResponseHeaders(200, response.getBytes().length);
                os.write(response.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        server.start();
    }
}
