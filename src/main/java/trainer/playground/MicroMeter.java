package trainer.playground;

import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.apache.logging.log4j.Logger;
import trainer.factory.Factory;

import java.util.ArrayList;
import java.util.List;

public class MicroMeter {
    private static final String GAUGE_NAME = "myGauge";
    private static final String COUNTER_NAME = "myCounter";

    public static void main(String[] args) {
        Logger log = Factory.getLogProvider().getLogger(MicroMeter.class);
        List<Integer> numbers = new ArrayList<>();
        SimpleMeterRegistry registry = new SimpleMeterRegistry();
        registry.gauge(GAUGE_NAME, numbers, values -> values.stream().mapToInt(i -> i).sum());
        registry.counter(COUNTER_NAME).increment(1.0);
        log.debug("Counter value is: {}", registry.get(COUNTER_NAME).counter().count());

        numbers.add(1);
        log.debug("Sum of list items is: {}", registry.get(GAUGE_NAME).gauge().value());
        numbers.addAll(List.of(2, 3, 4));
        log.debug("Sum of list items is: {}", registry.get(GAUGE_NAME).gauge().value());
        System.exit(0);
    }
}
