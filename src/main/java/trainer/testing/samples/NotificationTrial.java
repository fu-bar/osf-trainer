package trainer.testing.samples;

import osf.modules.notifications.Notification;
import osf.modules.notifications.NotificationManager;
import osf.modules.notifications.Notifier;
import trainer.factory.Factory;
import trainer.testing.structures.Trial;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class NotificationTrial extends Trial {
    public static final String notificationTopic = "notificationTopic";
    private final NotificationManager notificationManager = Factory.getNotificationManager();
    private final int numConsumers;
    private final int numNotifications;
    private final int numIterationsPerReport;
    private final List<CustomConsumer> consumerList = new ArrayList<>();

    public NotificationTrial(String trialId, Map<String, Object> trialParams) {
        super(trialId, trialParams);
        this.numConsumers = (int) trialParams.get("numConsumers");
        this.numNotifications = (int) trialParams.get("numNotifications");
        this.numIterationsPerReport = (int) trialParams.get("numIterationsPerRecord");
    }

    @Override
    public void init(Map<String, Object> trialParams) {
        for (int i = 0; i < this.numConsumers; i++) {
            this.consumerList.add(new CustomConsumer(this.notificationManager));
        }
    }

    @Override
    public void run() {
        for (int i = 0; i < this.numNotifications; i++) {
            notificationManager.publish(this, new Notification(notificationTopic, i));
            if (i % numIterationsPerReport == 0) {
                report("Published Notifications", i);
            }
        }
    }

    @Override
    public void cleanup() {
        report("Total Published", this.numNotifications);
        while (this.consumerList.stream().anyMatch(customConsumer -> customConsumer.countReceivedNotifications() < this.numNotifications)) {
            Map<String, Object> mapValues = new HashMap<>();
            for (int i = 0; i < this.consumerList.size(); i++) {
                mapValues.put("Consumer-" + i + " got", this.consumerList.get(i).countReceivedNotifications());
            }
            report("", mapValues);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
        Map<String, Object> mapValues = new HashMap<>();
        for (int i = 0; i < this.consumerList.size(); i++) {
            mapValues.put("Consumer-" + i + " got", this.consumerList.get(i).countReceivedNotifications());
        }
        report("", mapValues);
        this.consumerList.forEach(customConsumer -> customConsumer.stop());
    }

    class CustomConsumer implements Consumer<List<Notification>> {
        private final Notifier notifier;
        private final AtomicInteger notificationCounter = new AtomicInteger(0);

        public void stop() {
            this.notifier.stopPushing();
        }

        public CustomConsumer(NotificationManager notificationManager) {
            this.notifier = notificationManager.getNotifier(this);
            this.notifier.subscribeToTopic(NotificationTrial.notificationTopic);
            this.notifier.startRealtimePushing(this);
        }

        @Override
        public void accept(List<Notification> notifications) {
            for (int i = 0; i < notifications.size(); i++) {
                this.notificationCounter.incrementAndGet();
            }
        }

        public int countReceivedNotifications() {
            return this.notificationCounter.get();
        }
    }

}
