package trainer.testing.samples;

import trainer.testing.structures.Trial;

import java.util.Map;

public class FruitTrial extends Trial {
    private String fruitName;
    private int price;

    public FruitTrial(String trialId, Map<String, Object> trialParams) {
        super(trialId, trialParams);
    }

    @Override
    public void init(Map<String, Object> trialParams) {
        this.fruitName = (String) trialParams.get("fruitName");
        this.price = (int) trialParams.get("price");
        log.info("Fruit Trial - name: {}, price: {}", fruitName, price);
    }

    @Override
    public void run() {
        for (int i = 0; i < this.price; i++) {
            log.debug("The price of {} is not {}", this.fruitName, i);
            report("new price", i);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(-1);
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public void cleanup() {
        log.info("Trial {} is cleaning up", this.getTrialId());
    }
}
