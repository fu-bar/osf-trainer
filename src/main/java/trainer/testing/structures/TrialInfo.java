package trainer.testing.structures;

import java.util.Map;

public class TrialInfo {
    private String trialId;
    private Class<?> trialClass;
    private Map<String, Object> trialParams;

    public TrialInfo() {
        // Empty Constructor
    }

    public TrialInfo(String trialId, Class<?> trialClass, Map<String, Object> trialParams) {
        this.trialId = trialId;
        this.trialClass = trialClass;
        this.trialParams = Map.copyOf(trialParams);
    }

    public String getTrialId() {
        return trialId;
    }

    public void setTrialId(String trialId) {
        this.trialId = trialId;
    }

    public Map<String, Object> getTrialParams() {
        return trialParams;
    }

    public void setTrialParams(Map<String, Object> trialParams) {
        this.trialParams = trialParams;
    }

    public Class<?> getTrialClass() {
        return trialClass;
    }

    public void setTrialClass(Class<?> trialClass) {
        this.trialClass = trialClass;
    }
}
