package trainer.testing.structures;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import trainer.testing.exceptions.TrialException;

import java.util.Map;

public abstract class Trial {
    private final Recorder recorder;
    private final String trialId;
    protected final Logger log;
    private final Map<String, Object> trialParams;

    protected Trial(String trialId, Map<String, Object> trialParams) {
        this.trialId = trialId;
        this.trialParams = trialParams;
        this.log = LogManager.getLogger(this.trialId);
        this.recorder = new Recorder(trialId, trialParams);
        log.info("Instantiated trial {} with params: {}", trialId, trialParams);
    }

    /**
     * Record a timestamped <key,value> entry
     *
     * @param key   Entry category (think a column in a table)
     * @param value Entry value (think a value in the column)
     */
    public void report(String key, Object value) {
        this.recorder.record(key, value);
    }

    /**
     * Initialize and validate all pre-conditions before running the trial
     *
     * @param trialParams all parameters required to run the trial
     */
    public abstract void init(Map<String, Object> trialParams);

    public void start() {
        try {
            this.init(this.trialParams);
            this.recorder.startRecording();
            this.run();
            this.cleanup();
        } catch (Exception e) {
            throw new TrialException("Unexpected error while executing: " + this.trialId, e);
        }
    }

    public void stop() {
        this.recorder.stopRecording();
    }

    /**
     * Execute the main business logic of the trial
     */
    public abstract void run();

    /**
     * Finalize and clean resources after finishing running trial business logic
     */
    public abstract void cleanup();

    /**
     * Get trial recorder for post-processing
     *
     * @return Trial recorder
     */
    public Recorder getRecorder() {
        return new Recorder(this.recorder);
    }

    public String getTrialId() {
        return trialId;
    }
}
