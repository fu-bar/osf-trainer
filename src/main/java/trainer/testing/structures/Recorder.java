package trainer.testing.structures;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.collect.Tables;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;

public class Recorder {
    private final String id;
    private final Map<String, Object> trialParams;
    private final Table<Instant, String, Object> records;
    private final Logger log = LogManager.getLogger(Recorder.class);
    private final Instant instantiationTimestamp;
    private Instant startTimestamp;
    private Instant finishTimestamp;
    private Duration duration;
    private boolean isRecording = false;


    public Recorder(String id, Map<String, Object> trialParams) {
        this.id = id;
        this.instantiationTimestamp = Instant.now();
        this.trialParams = Map.copyOf(trialParams);
        this.records = HashBasedTable.create();
    }

    public Recorder(Recorder other) {
        this.id = other.getId();
        this.trialParams = other.getTrialParams();
        this.records = other.getRecords();
        this.instantiationTimestamp = other.getInstantiationTimestamp();
        this.startTimestamp = other.getStartTimestamp();
        this.finishTimestamp = other.getFinishTimestamp();
        this.duration = other.getDuration();
        this.isRecording = other.isRecording();
    }

    public void startRecording() {
        if (this.isRecording) {
            log.error("[IGNORING] Trial {} is already being recorded since {}", id, this.startTimestamp);
            return;
        }
        this.isRecording = true;
        this.startTimestamp = Instant.now();
        log.info("Started recording trial: {}", id);
    }

    public void stopRecording() {
        if (!this.isRecording) {
            log.error("[IGNORING] Trial is not being recorded");
            if (this.startTimestamp != null) {
                log.error("It has finished being recorded at: {}", this.finishTimestamp);
            }
            return;
        }
        this.finishTimestamp = Instant.now();
        this.duration = Duration.between(startTimestamp, finishTimestamp);
        this.isRecording = false;
        log.info("Finished recording trial: {}", id);
    }

    public void record(String key, Object value) {
        this.records.put(Instant.now(), key, value);
    }

    public Table<Instant, String, Object> getRecords() {
        return Tables.unmodifiableTable(this.records);
    }

    public String getId() {
        return id;
    }

    public Instant getInstantiationTimestamp() {
        return Instant.from(instantiationTimestamp);
    }

    public Instant getStartTimestamp() {
        return Instant.from(startTimestamp);
    }

    public Instant getFinishTimestamp() {
        return Instant.from(finishTimestamp);
    }

    public Duration getDuration() {
        return Duration.from(duration);
    }

    public boolean isRecording() {
        return isRecording;
    }

    public Map<String, Object> getTrialParams() {
        return Map.copyOf(this.trialParams);
    }


}
