package trainer.testing;

import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import trainer.testing.structures.Recorder;
import trainer.testing.structures.Trial;
import trainer.testing.structures.TrialInfo;
import trainer.utilities.Excel;
import trainer.utilities.JsonSerialization;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class TrialRunner {
    private static final Logger log = LogManager.getLogger(TrialRunner.class);
    private static List<TrialInfo> trialInfos;

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        printLogo();

        File outputFile = init(args);

        List<Recorder> collectedRecorders = new ArrayList<>();
        log.debug("Found {} defined trials", trialInfos.size());
        for (TrialInfo trialInfo : trialInfos) {
            Class<?> trialClass = trialInfo.getTrialClass();
            log.debug("Trial {} shall run {} with params {}",
                    trialInfo.getTrialId(),
                    trialInfo.getTrialClass(),
                    trialInfo.getTrialParams()
            );

            Trial trial = buildTrial(trialInfo, trialClass);
            runTrial(trial, collectedRecorders);
        }

        writeOutputsToExcelFile(outputFile, collectedRecorders);
    }

    private static void writeOutputsToExcelFile(File outputFile, List<Recorder> collectedRecorders) {
        log.info("Generating summary into: {}", outputFile.getAbsolutePath());
        try {
            if (!collectedRecorders.isEmpty()) {
                Excel.saveExcel(collectedRecorders, outputFile);
            } else {
                log.error("No collected recorders (did all trials fail?)");
            }
        } catch (Exception e) {
            log.error("Failed writing output file (is it open in Excel?): {}", outputFile.getAbsolutePath(), e);
        }
        log.debug("Finished writing into: {}", outputFile.getAbsolutePath());
    }

    private static void runTrial(Trial trial, List<Recorder> collectedRecorders) {
        log.debug("Running trial: {}", trial.getTrialId());
        try {
            trial.start();
            trial.stop();
            trial.cleanup();
        } catch (Exception e) {
            log.error("Unexpected exception while running {}", trial.getTrialId(), e);
            return;
        }
        collectedRecorders.add(trial.getRecorder());
        log.debug("Finished with trial: {}", trial.getTrialId());
    }

    @NotNull
    private static Trial buildTrial(TrialInfo trialInfo, Class<?> trialClass) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        log.debug("Instantiating trial: {} of class {} with params {}", trialInfo.getTrialId(), trialInfo.getTrialClass(), trialInfo.getTrialParams());
        Constructor<?> constructor = trialClass.getConstructor(String.class, Map.class);
        return (Trial) constructor.newInstance(trialInfo.getTrialId(), trialInfo.getTrialParams());
    }

    @NotNull
    private static File init(String[] args) {
        log.debug("Inspecting arguments: {}", Arrays.asList(args));

        if (args.length == 0) {
            log.error("[ABORTING] Usage parameters: <Trials JSON file> <Outputs Folder>");
            System.exit(-1);
        }

        // If output folder not specified, it shall be the working directory
        File outputFolder = new File(".");
        if (args.length > 1) {
            outputFolder = new File(args[1]);
            if (!Files.isDirectory(outputFolder.toPath())) {
                log.error("[ABORTING] Not a folder: {}", outputFolder.getAbsolutePath());
                System.exit(-1);
            }
        }

        File outputFile = Paths.get(outputFolder.getAbsolutePath(), "trial_" + new Date().getTime() + ".xlsx").toFile();

        File trialInfosFile = new File(args[0]);
        if (!Files.exists(trialInfosFile.toPath()) || Files.isDirectory(trialInfosFile.toPath())) {
            log.error("[ABORTING] Not a file: {}", trialInfosFile.getAbsolutePath());
            System.exit(-1);
        }

        try {
            trialInfos = JsonSerialization.objectMapper.readValue(trialInfosFile, new TypeReference<>() {
            });
        } catch (IOException e) {
            log.error("[ABORTING] Failed reading a list of TrialInfos from {}", trialInfosFile.getAbsolutePath());
            System.exit(-1);
        }
        return outputFile;
    }

    private static void printLogo() {
        log.info("\n" +
                "████████╗██████╗ ██╗ █████╗ ██╗                   ██████╗ ██╗   ██╗███╗   ██╗███╗   ██╗███████╗██████╗ \n" +
                "╚══██╔══╝██╔══██╗██║██╔══██╗██║                   ██╔══██╗██║   ██║████╗  ██║████╗  ██║██╔════╝██╔══██╗\n" +
                "   ██║   ██████╔╝██║███████║██║         █████╗    ██████╔╝██║   ██║██╔██╗ ██║██╔██╗ ██║█████╗  ██████╔╝\n" +
                "   ██║   ██╔══██╗██║██╔══██║██║         ╚════╝    ██╔══██╗██║   ██║██║╚██╗██║██║╚██╗██║██╔══╝  ██╔══██╗\n" +
                "   ██║   ██║  ██║██║██║  ██║███████╗              ██║  ██║╚██████╔╝██║ ╚████║██║ ╚████║███████╗██║  ██║\n" +
                "   ╚═╝   ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝╚══════╝              ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝");
    }
}
