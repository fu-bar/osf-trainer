package trainer.testing.exceptions;

public class TrialException extends RuntimeException {
    public TrialException(String errorMessage) {
        super(errorMessage);
    }

    public TrialException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
