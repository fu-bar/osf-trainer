package trainer.classes;

import org.apache.logging.log4j.Logger;
import trainer.factory.Factory;
import trainer.structures.NetworkTarget;
import trainer.structures.TCPMessage;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

public class TCPCommunicator extends TrainerClass implements Consumer<TCPMessage> {

    private final Set<ClientHandler> clientHandlers = new HashSet<>();
    private final BlockingQueue<TCPMessage> tcpMessages = new LinkedBlockingQueue<>(1000);
    private final Set<Consumer<TCPMessage>> messageConsumers = new HashSet<>();
    private ConnectionListener connectionListener;
    private MessageDispatcher messageDispatcher;
    private Socket socket;
    private ObjectOutputStream objectOutputStream;
    private OutputStream outputStream;
    private boolean isObjectOriented;
    private boolean isShutdownRequired = false;

    public TCPCommunicator() {
        this.isObjectOriented = false;
        this.log = Factory.getLogProvider().getLogger(TCPCommunicator.class);
    }

    public TCPCommunicator(boolean isObjectOriented) {
        this();
        this.isObjectOriented = isObjectOriented;
    }

    public void shutdown() {
        this.isShutdownRequired = true;
    }

    public void connect(NetworkTarget networkTarget) {
        try {
            this.socket = new Socket(networkTarget.getHost(), networkTarget.getPort());
            this.outputStream = socket.getOutputStream();
            this.objectOutputStream = new ObjectOutputStream(this.outputStream);
            log.debug("Client connected to {}:{}", networkTarget.getHost(), networkTarget.getPort());
        } catch (IOException e) {
            handleError(e);
        }
    }

    public void transmit(Serializable object) {
        try {
            if (this.isObjectOriented) {
                this.objectOutputStream.writeObject(object);
            } else {
                byte[] bytes = Utils.toBytes(object);
                this.outputStream.write(bytes);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addMessageConsumer(Consumer<TCPMessage> consumer) {
        synchronized (this) {
            this.messageConsumers.add(consumer);
        }
    }

    public void removeMessageConsumer(Consumer<TCPMessage> consumer) {
        synchronized (this) {
            this.messageConsumers.remove(consumer);
        }
    }

    public void transmit(TCPMessage tcpMessage) {
        Optional<ClientHandler> handler = this.clientHandlers.stream()
                .filter(clientHandler -> clientHandler.clientHandlerID.equals(tcpMessage.getClientId()))
                .findAny();
        if (handler.isEmpty()) {
            handleError(new RuntimeException(String.format("Failed locating handler with id: %s", tcpMessage.getClientId())));
        } else {
            ClientHandler clientHandler = handler.get();
            clientHandler.transmit(tcpMessage.getObject());
        }
    }

    public void startServer(int connectionPort) {
        this.connectionListener = new ConnectionListener(connectionPort, tcpMessages);
        this.connectionListener.start();
        this.messageDispatcher = new MessageDispatcher(messageConsumers, tcpMessages, this);
        this.messageDispatcher.start();
    }

    @Override
    public void accept(TCPMessage tcpMessage) {
        synchronized (this) {
            this.messageConsumers.forEach(consumer -> consumer.accept(tcpMessage));
        }
    }

    private class MessageDispatcher extends Thread {

        private final BlockingQueue<TCPMessage> tcpMessages;
        private final Set<Consumer<TCPMessage>> messageConsumers;
        private final Object lock;
        private boolean isShutdownRequired = false;


        public MessageDispatcher(Set<Consumer<TCPMessage>> messageConsumers, BlockingQueue<TCPMessage> tcpMessages, Object lock) {
            this.messageConsumers = messageConsumers;
            this.tcpMessages = tcpMessages;
            this.lock = lock;
        }

        public void shutdown() {
            this.isShutdownRequired = true;
        }

        @Override
        public void run() {
            while (!isShutdownRequired) {
                try {
                    TCPMessage tcpMessage = tcpMessages.take();
                    synchronized (lock) {
                        messageConsumers.forEach(consumer -> consumer.accept(tcpMessage));
                    }
                } catch (InterruptedException e) {
                    handleError(e);
                    this.interrupt();
                }
            }
        }
    }

    private class ClientHandler extends Thread {
        private final Socket socket;
        private final BlockingQueue<TCPMessage> tcpMessages;
        private final String clientHandlerID = "TCPCommunicator_" + this.getName();
        private final boolean isObjectOriented;
        private final InputStream inputStream;
        private final OutputStream outputStream;
        private final ObjectOutputStream objectOutputStream;
        private final ObjectInputStream objectInputStream;
        private boolean isShutdownRequired = false;

        public ClientHandler(Socket socket, BlockingQueue<TCPMessage> tcpMessages, boolean isObjectOriented) {
            this.socket = socket;
            this.tcpMessages = tcpMessages;
            this.isObjectOriented = isObjectOriented;
            try {
                this.inputStream = this.socket.getInputStream();
                this.objectInputStream = new ObjectInputStream(inputStream);
                this.outputStream = this.socket.getOutputStream();
                this.objectOutputStream = new ObjectOutputStream(outputStream);
            } catch (IOException e) {
                TrainerClass.handleStaticError(e);
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        public void transmit(Serializable object) {
            try {
                this.objectOutputStream.writeObject(object);
            } catch (IOException e) {
                log.debug("Transmitting object of type: {}", object.getClass().getName());
                handleError(e);
            }
        }

        public void shutdown() {
            log.debug("Shutting down");
            this.isShutdownRequired = true;
        }

        @Override
        public void run() {
            while (!isShutdownRequired) {
                Serializable object;
                try {
                    if (this.isObjectOriented) {
                        object = (Serializable) this.objectInputStream.readObject();
                    } else {
                        byte[] bytes = this.inputStream.readAllBytes();
                        object = Utils.fromBytes(bytes);
                    }
                    log.debug("Received an object of type: {}", object.getClass().getName());
                    this.tcpMessages.add(new TCPMessage(this.clientHandlerID, object));
                } catch (IOException | ClassNotFoundException e) {
                    handleError(e);
                }
            }
        }

    }

    private class ConnectionListener extends Thread {
        private final int connectionPort;
        private final Logger log;
        private ServerSocket serverSocket;

        public ConnectionListener(int connectionPort, BlockingQueue<TCPMessage> tcpMessages) {
            this.connectionPort = connectionPort;
            try {
                serverSocket = new ServerSocket(connectionPort);
            } catch (IOException e) {
                handleError(e);
            }
            this.log = Factory.getLogProvider().getLogger("ConnectionListener_" + this.getName());
        }

        @Override
        public void run() {
            this.log.info("Starting listening for TCP connections on port {}", connectionPort);
            while (!isShutdownRequired) {
                try {
                    Socket clientSocket = serverSocket.accept();
                    ClientHandler clientHandler = new ClientHandler(clientSocket, tcpMessages, isObjectOriented);
                    clientHandler.start();
                    clientHandlers.add(clientHandler);
                    this.log.debug("Accepted new connection from a client: {}", clientSocket.getRemoteSocketAddress());
                } catch (IOException e) {
                    handleError(e);
                }
            }

        }
    }
}
