package trainer.classes;

import osf.modules.synchronization.base.synchronization.RequestOperationInput;
import osf.modules.synchronization.base.synchronization.SynchronizationResult;
import osf.modules.synchronization.interfaces.OperationManager;

public class OperationManagerTrainer implements OperationManager {
    @Override
    public SynchronizationResult requestOperation(RequestOperationInput requestOperationInput) {
        return new SynchronizationResult(requestOperationInput.getToken(), SynchronizationResult.PolicyPermission.ALLOW);
    }
}
