package trainer.classes;

import osf.shared.interfaces.SystemInfoInterface;

public class SystemInfo implements SystemInfoInterface {
    @Override
    public String getServiceId() {
        return "trainerServiceId";
    }

    @Override
    public String getOSFInstanceId() {
        return "trainerOSFInstanceId";
    }

    @Override
    public String getOSFHomeFolder() {
        return "c:\\temp";
    }

    @Override
    public String getGlobalConfigurationNamespace() {
        return "GLOBAL::NAMESPACE";
    }

    @Override
    public String getServiceVersion() {
        return "1";
    }
}
