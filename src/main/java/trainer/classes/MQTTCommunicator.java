package trainer.classes;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import osf.shared.exceptions.OsfException;

import java.util.UUID;

public class MQTTCommunicator extends TrainerClass {
    private final String publisherId;
    private final MqttClient publisher;

    public MQTTCommunicator(String serverAddress) {
        this.publisherId = UUID.randomUUID().toString();
        try {
            this.publisher = new MqttClient(serverAddress, publisherId);
            MqttConnectOptions options = new MqttConnectOptions();
            options.setAutomaticReconnect(true);
            options.setCleanSession(true);
            options.setConnectionTimeout(10);
            publisher.connect(options);
        } catch (MqttException e) {
            log.error("Error while creating MQTT-Publisher for {}", serverAddress, e);
            throw new OsfException("Unexpected error while while creating MQTT-Publisher for " + serverAddress, e);
        }
    }


    public void sendMessage(String topic, byte[] payload) {
        MqttMessage mqttMessage = new MqttMessage(payload);
        try {
            publisher.publish(topic, mqttMessage);
        } catch (MqttException e) {
            log.error("Failed sending message on topic {}", topic, e);
            throw new OsfException("Unexpected error while sending message on topic " + topic, e);
        }
    }
}
