package trainer.classes;

import okhttp3.*;

public class RESTCommunicator extends TrainerClass {
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private final OkHttpClient client = new OkHttpClient();

    public Response post(String url, String jsonBody) {
        RequestBody body = RequestBody.create(jsonBody, JSON);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response;
        } catch (Exception e) {
            log.error("Unexpected error while posting to {}", url, e);
            return null;
        }
    }

    public Response get(String url) {
        Request request = new Request.Builder().url(url).build();
        try (Response response = client.newCall(request).execute()) {
            return response;
        } catch (Exception e) {
            log.error("Unexpected error while getting from {}", url, e);
            return null;
        }
    }

    public OkHttpClient getClient() {
        return client;
    }
}
