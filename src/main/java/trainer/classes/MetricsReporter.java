package trainer.classes;

import com.google.common.base.Stopwatch;
import osf.modules.elm.cache.MetricType;
import osf.modules.elm.cache.MetricsReporterInterface;

public class MetricsReporter implements MetricsReporterInterface {
    @Override
    public void reportTimingMetric(MetricType metricType, Stopwatch stopwatch) {

    }

    @Override
    public void reportCounterMetric(MetricType metricType, int increment) {

    }

    @Override
    public void reportValue(MetricType metricType, int value) {

    }
}
