package trainer.classes;

import osf.modules.contextprovider.ContextProvider;
import osf.shared.contextprovider.Outline;

import java.time.Duration;

public class ContextProviderTrainer implements ContextProvider {
    private final Outline outline;

    public ContextProviderTrainer() {
        this.outline = new Outline(
                "realityType",
                "realitySubType",
                "name",
                "outlineId",
                "exerciseId",
                "timeOrigin");
    }

    @Override
    public Duration getOTS() {
        return Duration.ZERO;
    }

    @Override
    public Outline getOutline() {
        return this.outline;
    }

    @Override
    public String getStateName() {
        return "Operational";
    }
}
