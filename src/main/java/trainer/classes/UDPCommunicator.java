package trainer.classes;

import org.apache.logging.log4j.LogManager;
import osf.modules.notifications.Notification;
import osf.modules.notifications.NotificationManager;
import trainer.structures.NetworkTarget;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import static trainer.structures.Constants.COMMUNICATOR_BUFFER_SIZE;

public class UDPCommunicator extends TrainerClass implements Runnable {

    private static final DatagramSocket transmittingSocket;

    static {
        try {
            transmittingSocket = new DatagramSocket();
        } catch (SocketException e) {
            handleStaticError(e);
            throw new RuntimeException("Failed initializing transmit-socket", e);
        }
    }

    private final Set<Consumer<byte[]>> byteConsumers = new HashSet<>();
    private final int listeningPort;
    private NotificationManager notificationManager;
    private DatagramSocket socket = null;
    private String notificationTopic;
    private boolean running;

    /**
     * UDP Communicator
     *
     * @param listeningPort       The port on which the communicator is listening
     * @param notificationManager The notification manager used by the owner of the communicator
     * @param notificationTopic   The topic on which the communicator shall publish incoming packets
     */
    public UDPCommunicator(int listeningPort, NotificationManager notificationManager, String notificationTopic) {
        this(listeningPort);
        this.notificationManager = notificationManager;
        this.notificationTopic = notificationTopic;
        try {
            this.socket = new DatagramSocket(this.listeningPort);
        } catch (SocketException e) {
            handleError(e);
        }
    }

    public UDPCommunicator(int listeningPort, Consumer<byte[]> byteConsumer) {
        this(listeningPort);
        this.byteConsumers.add(byteConsumer);
    }

    public UDPCommunicator(int listeningPort) {
        this.log = LogManager.getLogger(String.format("UDP PORT: %d", listeningPort));
        this.listeningPort = listeningPort;
    }

    /**
     * Send a serializable object as a byte-array to BUS target
     *
     * @param serializable  The object to be serialized and sent
     * @param networkTarget The host and port the byte-array to be sent to
     */
    public static void send(Serializable serializable, NetworkTarget networkTarget) {
        byte[] bytes = Utils.toBytes(serializable);
        DatagramPacket sendPacket = new DatagramPacket(bytes, bytes.length, new InetSocketAddress(networkTarget.getHost(), networkTarget.getPort()));
        try {
            transmittingSocket.send(sendPacket);
        } catch (IOException e) {
            handleStaticError(e);
        }
    }

    public void startNotifications(NotificationManager notificationManager, String notificationTopic) {
        this.notificationManager = notificationManager;
        this.notificationTopic = notificationTopic;
    }

    public void stopNotifications() {
        this.notificationManager = null;
        this.notificationTopic = null;
    }

    public void registerByteConsumer(Consumer<byte[]> consumer) {
        synchronized (this) {
            this.byteConsumers.add(consumer);
        }
    }

    public void unregisterByteConsumer(Consumer<byte[]> consumer) {
        synchronized (this) {
            this.byteConsumers.remove(consumer);
        }
    }

    /**
     * Stop listening loop
     */
    public void stopListening() {
        log.debug("Stopping listening loop");
        this.running = false;
    }

    @Override
    public void run() {
        log.debug("Listening on port {} (will send topic: {})", listeningPort, notificationTopic);
        running = true;

        byte[] buf = new byte[COMMUNICATOR_BUFFER_SIZE];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);

        while (running) {

            try {
                socket.receive(packet);
            } catch (IOException e) {
                handleError(e);
            }

            byte[] receivedBytes = Arrays.copyOfRange(packet.getData(), 0, packet.getLength());
            boolean allBytesZero = true;
            for (byte b : receivedBytes) {
                allBytesZero &= (b == 0);
            }

            if (allBytesZero) {
                log.debug("Received all bytes as zero : stopping");
                running = false;
                continue;
            }
            synchronized (this) {
                for (var byteConsumer : this.byteConsumers) {
                    byteConsumer.accept(receivedBytes);
                }
            }
            if (this.notificationManager != null && this.notificationTopic != null) {
                notificationManager.publish(this, new Notification(notificationTopic, receivedBytes));
            }
        }
        socket.close();
    }
}
