package trainer.classes;

import osf.bus.shared.channel.exceptions.BusCommunicationException;
import osf.bus.shared.message.types.OSFMessage;
import osf.bus.shared.message.utility.OSFMessageFilterInterface;
import osf.modules.bus.api.shared.SystemBusInterface;
import osf.modules.notifications.Notification;
import osf.modules.notifications.NotificationManager;
import osf.modules.notifications.Topics;
import osf.objects.OSFOperator;
import trainer.structures.NetworkTarget;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

@SuppressWarnings("unused")
public class Bus extends TrainerClass implements SystemBusInterface, Consumer<byte[]> {

    private final Set<Consumer<OSFMessage>> messageConsumers = new HashSet<>();
    private final Set<Consumer<List<Notification>>> notificationConsumers = new HashSet<>();
    private final Set<NetworkTarget> networkTargets = new HashSet<>();
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private UDPCommunicator udpCommunicator;
    private NotificationManager notificationManager;
    private String notificationTopic;

    public Bus(NotificationManager notificationManager, String notificationTopic) {
        this.notificationManager = notificationManager;
        this.notificationTopic = notificationTopic;
    }

    public Bus(Consumer<OSFMessage> consumer) {
        this.messageConsumers.add(consumer);
    }

    /**
     * Add a target to message-transmission list
     *
     * @param networkTarget Represents a target all messages will be sent to
     */
    public void addBusTarget(NetworkTarget networkTarget) {
        log.debug("Adding bus-target: host={} port={}", networkTarget.getHost(), networkTarget.getPort());
        synchronized (this) {
            this.networkTargets.add(networkTarget);
        }
    }

    /**
     * Remove a target from message-transmission list
     *
     * @param networkTarget The target to be removed
     */
    public void removeBusTarget(NetworkTarget networkTarget) {
        log.debug("Removing bus-target: host={} port={}", networkTarget.getHost(), networkTarget.getPort());
        synchronized (this) {
            this.networkTargets.remove(networkTarget);
        }
    }

    /**
     * Register an OSFMessage consumer
     * Mainly to be used for inspecting sent messages during debug/sandbox execution
     *
     * @param consumer The consumer to register
     */
    public void addOSFMessageConsumer(Consumer<OSFMessage> consumer) {
        log.debug("Adding message consumer");
        synchronized (this) {
            this.messageConsumers.add(consumer);
        }
    }

    public void removeOSFMessageConsumer(Consumer<OSFMessage> consumer) {
        log.debug("Removing message consumer");
        synchronized (this) {
            this.messageConsumers.remove(consumer);
        }
    }

    public void addNotificationConsumer(Consumer<List<Notification>> consumer) {
        log.debug("Adding notification consumer");
        synchronized (this) {
            this.notificationConsumers.add(consumer);
        }
    }

    public void removeNotificationConsumer(Consumer<List<Notification>> consumer) {
        log.debug("Removing notification consumer");
        synchronized (this) {
            this.notificationConsumers.remove(consumer);
        }
    }

    /**
     * Register a notification manager along with a topic to be used for publishing incoming OSF-Messages
     *
     * @param notificationManager Notification manager to be used
     * @param notificationTopic   Notification topic to be set for every published notification
     */
    public void startNotifying(NotificationManager notificationManager, String notificationTopic) {
        synchronized (this) {
            this.notificationManager = notificationManager;
            this.notificationTopic = notificationTopic;
        }
    }

    /**
     * Stop publishing notifications for incoming osf-messages
     */
    public void stopNotifying() {
        synchronized (this) {
            this.notificationManager = null;
            this.notificationTopic = null;
        }
    }

    @Override
    public <T extends OSFMessage> T createMessage(Class<T> aClass, OSFOperator osfOperator) {
        try {
            Constructor<T> constructor = aClass.getConstructor();
            return constructor.newInstance();
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            handleError(e);
        }
        return null;
    }

    @Override
    public void setFilter(OSFMessageFilterInterface osfMessageFilterInterface) {
        // Nothing here
    }

    @Override
    public void startCommunication() throws BusCommunicationException {
        // Nothing here
    }

    @Override
    public boolean stopCommunication() {
        return true;
    }

    @Override
    public boolean sendMessage(OSFMessage osfMessage) {
        log.debug("Sending OSF BUS message");
        int countConsumers = this.messageConsumers.size();
        int countBusTargets = this.networkTargets.size();
        if (countConsumers > 0) {
            log.debug("Dispatching bus-message to {} consumers", countConsumers);
            synchronized (this) {
                for (var consumer : this.messageConsumers) {
                    consumer.accept(osfMessage);
                }
                for (var consumer : this.notificationConsumers) {
                    consumer.accept(List.of(new Notification(Topics.BUS.OSF_MESSAGE, osfMessage)));
                }
            }
        }
        if (countBusTargets > 0) {
            log.debug("Transmitting bus-message to {} targets", countBusTargets);
            synchronized (this) {
                for (var busTarget : networkTargets) {
                    UDPCommunicator.send(osfMessage, busTarget);
                }
            }

        }
        if (isNotifyingOn()) {
            log.debug("Dispatching a notification with the sent sent as data on topic {}", this.notificationTopic);
            Notification notification = new Notification(this.notificationTopic, osfMessage);
            this.notificationManager.publish(this, notification);
        }
        return true;
    }

    /**
     * Start listening for incoming communication
     *
     * @param port Listening port
     */
    public void startPortListening(int port) {
        if (this.udpCommunicator != null) {
            stopPortListening();
        }
        this.udpCommunicator = new UDPCommunicator(port);
        this.udpCommunicator.registerByteConsumer(this);
        executorService.submit(this.udpCommunicator);
    }

    /**
     * Stop listening on currently listened upon port
     */
    public void stopPortListening() {
        log.debug("Stopping active listening thread");
        this.udpCommunicator.stopListening();
    }

    @Override
    public void accept(byte[] bytes) {
        Object deserialized = Utils.fromBytes(bytes);
        if (!OSFMessage.class.isAssignableFrom(deserialized.getClass())) {
            log.error("Received {} bytes are expected to be an OSFMessage but are actually {}", bytes.length, deserialized.getClass());
            return;
        }
        OSFMessage osfMessage = (OSFMessage) deserialized;
        synchronized (this) {
            if (isNotifyingOn()) {
                this.notificationManager.publish(this, new Notification(this.notificationTopic, osfMessage));
            }
            for (var messageConsumer : this.messageConsumers) {
                messageConsumer.accept(osfMessage);
            }
        }
    }

    private boolean isNotifyingOn() {
        return this.notificationManager != null && this.notificationTopic != null;
    }
}
