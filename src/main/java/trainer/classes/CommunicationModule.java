package trainer.classes;

import org.apache.logging.log4j.Logger;
import ota.communication.shared.interfaces.CommunicationModuleInterface;
import ota.communication.shared.types.TestFunctionality;
import ota.communication.shared.types.TestOutput;
import trainer.factory.Factory;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class CommunicationModule extends TrainerClass implements CommunicationModuleInterface {
    private final SystemInfo systemInfo = new SystemInfo();
    private final Set<Consumer<TestOutput>> outputConsumers = new HashSet<>();
    private final Logger log;

    public CommunicationModule() {
        this.log = Factory.getLogProvider().getLogger(CommunicationModule.class);
    }

    public void registerOutputConsumer(Consumer<TestOutput> consumer) {
        synchronized (this) {
            outputConsumers.add(consumer);
        }
    }

    public void unRegisterOutputConsumer(Consumer<TestOutput> consumer) {
        synchronized (this) {
            outputConsumers.remove(consumer);
        }
    }

    @Override
    public void registerModuleForAction(String s, Consumer<Object> consumer) {

    }

    @Override
    public void registerModuleFunctionality(TestFunctionality.TestModule testModule) {

    }

    @Override
    public void writeOutput(TestOutput testOutput) {
        String json = Utils.toJson(testOutput);
        log.info(json);
        synchronized (this) {
            this.outputConsumers.forEach(consumer -> consumer.accept(testOutput));
        }
    }

    @Override
    public String getAppName() {
        return systemInfo.getServiceId();
    }
}
