package trainer.classes;

import osf.modules.synchronization.interfaces.TimeProviderBase;

import java.time.Instant;

public class TimeProvider implements TimeProviderBase {
    @Override
    public Instant getOperationalTime() {
        return Instant.now();
    }

    @Override
    public Instant getSystemTime() {
        return Instant.now();
    }
}
