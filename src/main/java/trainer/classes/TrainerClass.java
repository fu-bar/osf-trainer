package trainer.classes;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import trainer.factory.Factory;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class TrainerClass {
    private final Set<Consumer<Throwable>> errorHandlers = new HashSet<>();
    protected Logger log;

    public TrainerClass() {
        this.log = Factory.getLogProvider().getLogger(this.getClass());
    }

    public static void handleStaticError(Throwable error) {
        String stacktrace = ExceptionUtils.getStackTrace(error);
        System.out.println(stacktrace);
    }

    public void handleError(Throwable error) {
        String stacktrace = ExceptionUtils.getStackTrace(error);
        this.log.error(stacktrace);
        errorHandlers.forEach(handler -> handler.accept(error));
    }

    public void addErrorHandler(Consumer<Throwable> errorHandler) {
        synchronized (this) {
            this.errorHandlers.add(errorHandler);
        }
    }

    public void removeErrorHandler(Consumer<Throwable> errorHandler) {
        synchronized (this) {
            this.errorHandlers.remove(errorHandler);
        }
    }
}
