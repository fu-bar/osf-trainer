package trainer.classes;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;

public class Utils {
    private final static ObjectMapper objectMapper = new ObjectMapper();

    static {
        Utils.objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        Utils.objectMapper.registerModule(new JavaTimeModule());
        Utils.objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    }

    private Utils() {
        // Static
    }

    public static String toJson(Object object, boolean isPretty) {
        try {
            if (isPretty) {
                return Utils.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
            } else {
                return Utils.objectMapper.writeValueAsString(object);
            }
        } catch (JsonProcessingException e) {
            TrainerClass.handleStaticError(e);
        }
        return null;
    }

    public static String toJson(Object object) {
        return toJson(object, true);
    }

    public static byte[] toBytes(Object object) {
        return SerializationUtils.serialize((Serializable) object);
    }

    public static <T> T fromBytes(byte[] bytes) {
        return SerializationUtils.deserialize(bytes);
    }
}
